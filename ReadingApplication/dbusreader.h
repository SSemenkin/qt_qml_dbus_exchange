#ifndef DBUSREADER_H
#define DBUSREADER_H

#include <QObject>
#include <QThread>

#include "../global_defines.h"


class DBusReader : public QObject
{
    Q_OBJECT
public:
    explicit DBusReader(QObject *parent = nullptr);
    ~DBusReader();

    void checkForUpdates();

signals:
    void appendText(const QString &text);

private:
    QThread *m_thread;
    QDBusInterface m_DBusinterface;

    QMutex m_mutex;
    bool m_break {false};
};

#endif // DBUSREADER_H
