#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "dbusreader.h"
#include "mainthreadsignalemitter.h"


int main(int argc, char *argv[])
{ 
    if(!QDBusConnection::sessionBus().isConnected()) {
        qDebug() << "Cannot connect to the D-Bus Session Bus";
        return -1;
    }

    QDBusReply<QStringList> services = QDBusConnection::sessionBus().interface()->registeredServiceNames();

    if(!services.value().contains(SERVICE_NAME)) {
        bool result = registerService();
        if (!result) {
            return -2;
        }
    }

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    DBusReader reader;
    MainThreadSignalEmitter object;
    QObject::connect(&reader, &DBusReader::appendText, &object, &MainThreadSignalEmitter::textAppended);

    engine.rootContext()->setContextProperty("textGetter", &object);
    return app.exec();
}
