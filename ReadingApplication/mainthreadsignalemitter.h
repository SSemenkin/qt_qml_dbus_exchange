#ifndef MAINTHREADSIGNALEMITTER_H
#define MAINTHREADSIGNALEMITTER_H

#include <QObject>

class MainThreadSignalEmitter : public QObject
{
    Q_OBJECT
public:
    explicit MainThreadSignalEmitter(QObject *parent = nullptr);

signals:
    void textAppended(const QString &);

};

#endif // MAINTHREADSIGNALEMITTER_H
