#include "dbusreader.h"

DBusReader::DBusReader( QObject *parent)
    : QObject{parent},
      m_thread(new QThread),
      m_DBusinterface(SERVICE_NAME, "/", "", QDBusConnection::sessionBus())
{
    moveToThread(m_thread);
    connect(m_thread, &QThread::started, this, &DBusReader::checkForUpdates);
    connect(m_thread, &QThread::finished, m_thread, &QThread::deleteLater);
    m_thread->start();
}

DBusReader::~DBusReader()
{
    QMutexLocker locker(&m_mutex);
    m_break = true;
}

void DBusReader::checkForUpdates()
{
   while(!m_break) {
        QMutexLocker locker(&m_mutex);
        if (m_DBusinterface.isValid()) {
             QDBusReply<QString> reply = m_DBusinterface.call("getLastMessage");
             if (reply.isValid() && !reply.value().isEmpty()) {
                 emit appendText(reply.value());
             }
        }
    }
    m_thread->quit();
}
