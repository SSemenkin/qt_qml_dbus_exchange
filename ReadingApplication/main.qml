import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("QDBus Reader")



    TextField {
        id: field
        anchors.centerIn: parent

        Connections {
            target: textGetter

            function onTextAppended(text) {
                field.text = field.text + text
            }
        }

    }
}
