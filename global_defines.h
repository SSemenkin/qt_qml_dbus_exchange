#ifndef GLOBAL_DEFINES_H
#define GLOBAL_DEFINES_H
#include <QtDBus/QtDBus>

#define SERVICE_NAME "test.DBus.qml.exchange"

static bool registerService()
{
    bool ok = QDBusConnection::sessionBus().registerService(SERVICE_NAME);
    if(!ok) {
        qDebug() << QString("Cannot register %1").arg(SERVICE_NAME);
        qDebug() << QDBusConnection::sessionBus().lastError();
    } else {
        qDebug() << "The service has been successfully registered.";
    }
    return ok;
}

#endif // GLOBAL_DEFINES_H
