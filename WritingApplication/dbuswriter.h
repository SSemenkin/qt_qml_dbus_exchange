#ifndef DBUSWRITER_H
#define DBUSWRITER_H

#include <QObject>

class DBusWriter : public QObject
{
    Q_OBJECT
public:
    explicit DBusWriter(QObject *parent = nullptr);

public slots:
    Q_INVOKABLE void write(const QString &text);
    Q_INVOKABLE QString getLastMessage();

signals:

private:
    QStringList m_messages;

};

#endif // DBUSWRITER_H
