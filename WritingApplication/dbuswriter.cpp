#include "dbuswriter.h"
#include <QDebug>

DBusWriter::DBusWriter(QObject *parent)
    : QObject{parent}
{

}

void DBusWriter::write(const QString &text)
{
    qDebug() << Q_FUNC_INFO << text;
    m_messages << text;
}

QString DBusWriter::getLastMessage()
{
    QString result;
    if (!m_messages.isEmpty()) {
        result = m_messages.last();
        m_messages.pop_back();
    }
    return result;
}
