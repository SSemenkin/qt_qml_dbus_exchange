import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("QDBus Writer")

    Column {
        anchors.centerIn: parent
        spacing: 10
        Button {
            id: btn
            text: "1"
            onClicked: function() {
                DBusWriter.write(text);
            }
        }
        Button {
            text: "2"
            onClicked: function() {
                DBusWriter.write(text);
            }
        }
        Button {
            text: "3"
            onClicked: function() {
                DBusWriter.write(text);
            }
        }
    }
}
