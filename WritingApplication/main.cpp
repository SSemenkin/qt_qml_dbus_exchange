#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QtDBus/QtDBus>

#include "../global_defines.h"
#include "dbuswriter.h"

int main(int argc, char *argv[])
{
    if(!QDBusConnection::sessionBus().isConnected()) {
        qDebug() << "Cannot connect to the D-Bus Session Bus";
        return -1;
    }

    QDBusReply<QStringList> services = QDBusConnection::sessionBus().interface()->registeredServiceNames();

    if(!services.value().contains(SERVICE_NAME)) {
        bool result = registerService();
        if (!result) {
            return -2;
        }
    }

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    DBusWriter writer;
    engine.rootContext()->setContextProperty("DBusWriter", &writer);

    QDBusConnection::sessionBus().registerObject("/", &writer, QDBusConnection::RegisterOption::ExportAllSlots);

    return app.exec();
}
